#ifndef ROSSERIAL_lib__H_
#define ROSSERIAL_lib__H_

#ifdef __cplusplus
 extern "C" {
#endif

void Setup(void);
void Loop(void);

#ifdef __cplusplus
}
#endif
#endif
