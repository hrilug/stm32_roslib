#include "rosserial_lib.h"
#include "cmsis_os.h"
#include <ros.h>
#include <std_msgs/String.h>

ros::NodeHandle nh;
std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);

char hello[] = "Hello world!";

void Setup(void) {
    nh.initNode();
    nh.advertise(chatter);
}

void Loop(void) {
    HAL_GPIO_TogglePin(GPIOF, GPIO_PIN_9);
    str_msg.data = hello;
    chatter.publish(&str_msg);
    nh.spinOnce();
    HAL_Delay(500);
}
