# stm32_roslib库

## 基本信息
1.版本及作者信息：

* Hrilug（408-RC）
* V1.3
* 2024.5.4

2.内容：
* 概述：使用rosserial_stm32功能包build的用于stm32-VCP的库
* 教程：https://www.bilibili.com/video/BV1A1421D7VT
* 参考：https://github.com/yoneken/rosserial_stm32

## 文件概览
### 一、增加文件
* rosserial_lib.cpp：用于用户自定义的ros通信文件
* rosserial_lib.h：rosserial_lib的头文件，同时声明c/c++文件混合编译
### 二、修改文件
* STM32Hardware.h：将usart接口修改为usb的vcp接口
* usbd_cdc_if.c(替换或修改HAL库文件)：修改HAL_USB中断接口，增加消息回环

## stm32CubeIDE配置
* 使用c++类型创建CubeIDE项目
* RCC外部晶振，高速和低速都打开
* Connectivity内打开USB_OTG_FS，配置为Device_Only模式，打开NVIC中断
* 配置时钟图，确保USB_FS的时钟为48M
* 配置USB_VCP虚拟端口
* 配置FreeRTOS，并设置用于ROS通信的任务大小为静态同时至少为3000
* Project Manager配置LinkerSettings最小Heap Size为8X800（其实我也不知道有什么作用，但是有的博客确实提到要设置）
* Project Manager配置文件为输出.c/.h文件

## stm32_roslib库文件使用
一、前提：使用前提是你已经安装好了ros和rosserial功能包（用于ros通信的agent代理），具体使用教程可以参考408内部文件「rosserial从入门到入坟」

二、先创建一个CubeIDE项目文件，在项目文件夹中clone本库，在CubeIDE的Incude导入该文件夹，参考文档修改/USB_DEVICE/App下的usbd_cdc_if.c文件，修改内容参照本库中的_usbd_cdc_if.c文件

三、修改freerots.c文件，导入“rosserial_lib.h”头文件，并在你Freertos的Task中使用rosserial_lib.h的函数，例如下：

    /* USER CODE END Header_StartDefaultTask */
    void StartDefaultTask(void *argument)
    {
      /* init code for USB_DEVICE */
      MX_USB_DEVICE_Init();
      /* USER CODE BEGIN StartDefaultTask */
     osDelay(500);
     Setup();
     /* Infinite loop */
     osDelay(500);
     for(;;)
     {
      Loop();
     }
     /* USER CODE END StartDefaultTask */
    }

